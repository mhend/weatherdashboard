import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google-map';
import _ from "lodash";

class WeatherList extends Component {

    renderWeather(cityData){
        console.log(cityData.list)
        const name = cityData.city.name;
        const temps = cityData.list.map(weather => weather.main.temp);
        const pressure = cityData.list.map(weather => weather.main.pressure);
        const humidity = cityData.list.map(weather => weather.main.humidity);
        const { lon, lat } = cityData.city.coord;

        console.log(temps);
        return (
            <tr key={name}>
                <td><GoogleMap lon={lon} lat={lat} /></td>
                <td>
                    <Chart data={temps} color="orange" avg={convert(average(temps))} units="F"/>
                </td>
                <td>
                    <Chart data={pressure} color="blue" avg={average(pressure)} units="hPa"/>
                </td>
                <td>
                    <Chart data={humidity} color="green" avg={average(humidity)} units="%"/>
                </td>
            </tr>
        )
    }

    render() {
        return(
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature (F)</th>
                        <th>Pressure (hPa)</th>
                        <th>Humidity (%)</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

function average(data) {
    return _.round(_.sum(data)/data.length);
}
function convert(data){
    return (9/5) * (data - 273) + 32;
}

function mapStateToProps({ weather }) {
    return { weather };
}

export default connect(mapStateToProps)(WeatherList);